RP
=========

Installation et configuration de la revue de press de LQDN ( version 2 )


Requirements
------------

Aucunes.

Role Variables
--------------

### Utilisateurice RP
`rp_domain` : Le nom de domaine où sera accessible la RP. Ex : "rp.lqdn.fr"
`rp_unix_user` : Le nom d'utilisateurice du service. Ex: "rp"
`rp_home_path` : Le home de l'utilisateurice précédente. Ex: "/opt/rp/"

### Code source
`rp_version` : La version de la RP à installer. Ex:  "v0.0.1"
`rp_download_url`: L'URL d'où installer la RP. Ex: "https://git.laquadrature.net/la-quadrature-du-net/rpteam/rp/-/archive/{{ rp_version }}/rp-{{ rp_version }}.zip"
`rp_source_path`: Là où sera installer la rp. Ex: "{{ rp_home_path }}/rp2"


`npm_packages`: Les paquets npm à installer.

### Réglages Django
`rp_debug` : Activer le mode debug : "True" ou "False"
`rp_django_secret_key` : La clé secrète de l'installation Django ⚠ Information sensible Ex : "fce8ed01ea24182c287864c6372f43bd276867bbd5b6491f2e8bc472ab4ee1d81a3e93c6d7ee290b93cdcc9da8cb5ce9089c6358de43535e6225eb4ec7f05b78"
`rp_django_site_id` : L'ID du site. Ex : "1"
`rp_django_allowed_hosts` : Si le monde débug est désactivé, permet d'accepter les connections pour un domaine. Ex : "{{ rp_domain }}"

Dependencies
------------

Aucunes.

Example Playbook
----------------

License
-------

BSD

Author Information
------------------

An optional section for the role authors to include contact information, or a website (HTML is not allowed).

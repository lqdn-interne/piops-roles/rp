---
- name: Creating a rp user
  user:
      append: yes
      groups: [www-data]
      home: "{{ rp_home_path }}"
      name: "{{ rp_unix_user }}"
      state: present
      system: yes
      password: '*'

- name: Installing the required nodejs packages
  npm:
      global: yes
      production: "{% if rp_debug %}false{% else %}true{% endif %}"
      name: "{{ item }}"
      state: latest
  loop: "{{ npm_packages }}"

- name: Installing uwsgi (emperor) packages
  package:
      name: "{{ item }}"
      state: latest
  loop:
      - uwsgi-emperor
      - uwsgi-plugin-python3

- name: Enabling uwsgi (emperor) on start
  service:
      name: uwsgi-emperor
      enabled: yes
      state: started

- name: Creating the /run/ directory
  file:
      state: directory
      path: /run/uwsgi/app
      owner: "{{ rp_unix_user }}"
      group: www-data
      mode: g+srw

- name: Creating the directory for the source code
  file:
      path: "{{ rp_source_path }}"
      state: directory
      group: www-data
      owner: "{{ rp_unix_user }}"
      mode: g+srw
      recurse: yes

- block:
    - name: Decompress the archive of rp's source code
      unarchive:
          src: "{{ rp_download_url }}"
          dest: "{{ rp_source_path }}"
          keep_newer: yes
          remote_src: yes
          mode: g+srw
          owner: "{{ rp_unix_user }}"
          group: www-data
          # Will remove
          extra_opts: [--strip-components=1]

    - name: install python requirement
      pip:
          requirements: "{{ rp_source_path }}/requirements.txt"
          virtualenv: "{{ rp_source_path }}/env"
          virtualenv_python: python3

    - name: install python dev requirements
      pip:
          requirements: "{{ rp_source_path }}/requirements-dev.txt"
          virtualenv: "{{ rp_source_path }}/env"
          virtualenv_python: python3
      when: rp_debug
  become: yes
  become_user: "{{ rp_unix_user }}"

- name: setting proper permission for the virtualenv
  file:
      path: "{{ rp_source_path }}/env"
      state: directory
      follow: false
      group: www-data
      mode: g+srw
      recurse: yes

- block:
    - name: Install the application's dependencies with yarn
      yarn:
          path: "{{ rp_source_path }}/"
          state: latest
          production: yes
      register: yarn_out
      changed_when: '"Already up-to-date." not in yarn_out.out'

    # - name: Install webpack with yarn
    #   shell:
    #       path: "{{ rp_source_path }}/"
    #       name: webpack
    #       state: latest
    #       production: yes
    - name: Installing webpack with yarn
      shell: "yarn add webpack"
      args:
        chdir: "{{ rp_source_path }}/"

    - name: Webpack the css
      shell: "yarn run webpack"
      args:
        chdir: "{{ rp_source_path }}/"
      changed_when: false

    - name: create the local env settings for django
      template:
          src: templates/env.py.j2
          dest: "{{ rp_source_path }}/project/settings/env.py"
          owner: "{{ rp_unix_user }}"
          group: www-data
          mode: g+srw

    - name: Migrate the database
      django_manage:
          command: migrate
          virtualenv: "{{ rp_source_path }}/env/"
          pythonpath: "{{ rp_source_path }}/project"
          settings: project.settings
          app_path: "{{ rp_source_path }}"

    - name: collect static file for django
      django_manage:
          command: collectstatic
          link: true
          virtualenv: "{{ rp_source_path }}/env/"
          pythonpath: "{{ rp_source_path }}/project"
          settings: project.settings
          app_path: "{{ rp_source_path }}"

  become: yes
  become_user: "{{ rp_unix_user }}"

- name: Adds a vassal for rp
  template:
      src: templates/vassals-rp.j2
      dest: /etc/uwsgi-emperor/vassals/rp.ini
      group: www-data
      mode: g+rw

- name: Configuration du fichier SystemD
  template:
    src: templates/rp2.service.j2
    dest: /etc/systemd/system/rp2.service
    owner: root
    group: root
    mode: 0644
